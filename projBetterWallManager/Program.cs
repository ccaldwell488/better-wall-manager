﻿using System;
using System.Windows.Forms;

namespace projBetterWallManager
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            frmMonitorCount mon = new frmMonitorCount();
            if (mon.ShowDialog() == DialogResult.OK)
            {
                Application.Run(new frmWallpaperTool(mon.selection));
            }
            else
            {
                Application.Exit();
            }
        }
    }
}
