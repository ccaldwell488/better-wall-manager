﻿using System.Windows.Forms;

namespace projBetterWallManager
{
    // This class allows for the dynamic creation of pictureBox objects containing drag and drop functions
    class CustomPictureBox : PictureBox
    {
        public override bool AllowDrop { get; set; }

        //Random rnd = new Random();
        //byte[] ARGB = new byte[4];
        static frmWallpaperTool instance;

        public CustomPictureBox(frmWallpaperTool _instance)
        {
            instance = _instance;
            int w = 220;
            int h = 220;

            Size = new System.Drawing.Size(w , h);
            BackColor = System.Drawing.Color.BlueViolet;

            AllowDrop = true;
            DragEnter += CustomPictureBox_DragEnter;
            DragDrop += CustomPictureBox_DragDrop;
        }

        public void CustomPictureBox_DragEnter(object sender , DragEventArgs e)
        {
            // Display cursor effects for visual feedback
            e.Effect = DragDropEffects.All;
        }

        // Display image dropped within picbox and txtbox on the form
        public void CustomPictureBox_DragDrop(object sender , DragEventArgs e)
        {
            // TODO exception testing
            string[] foo = (string[])e.Data.GetData(DataFormats.FileDrop , false);
            if (foo[0].EndsWith("jpg") || foo[0].EndsWith("png") || foo[0].EndsWith("bmp"))
            {
                this.ImageLocation = foo[0];
                instance.itemDropped(foo[0]);
            }
            foo = null;
        }
    }
}
