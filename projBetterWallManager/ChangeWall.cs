﻿using Microsoft.Win32;

public class ChangeWall
{
    // Gets and sets registry values of the windows desktop background
    public ChangeWall()
    {

    }

    public string getCurrentWallpaper()
    {
        return (string)Registry.GetValue(@"HKEY_CURRENT_USER\Control Panel\Desktop" , "Wallpaper" , "error");
    }

    public object setCurrentWallpaper(string value)
    {
        Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\Desktop" , "Wallpaper" , value);
        return getCurrentWallpaper();
    }

    public void setWallMode(int value)
    {
        Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\Desktop" , "WallpaperStyle" , value);
    }

    public string getCurrentMode()
    {
        return (string)Registry.GetValue(@"HKEY_CURRENT_USER\Control Panel\Desktop" , "WallpaperStyle" , "error");
    }
}
