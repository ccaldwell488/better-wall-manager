﻿namespace projBetterWallManager
{
    partial class frmWallpaperTool
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSet = new System.Windows.Forms.Button();
            this.flp = new System.Windows.Forms.FlowLayoutPanel();
            this.cboMode = new System.Windows.Forms.ComboBox();
            this.pnlControls = new System.Windows.Forms.Panel();
            this.txtPath = new System.Windows.Forms.TextBox();
            this.lblCurrent = new System.Windows.Forms.Label();
            this.pnlControls.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnSet
            // 
            this.btnSet.Location = new System.Drawing.Point(3, 16);
            this.btnSet.Name = "btnSet";
            this.btnSet.Size = new System.Drawing.Size(113, 23);
            this.btnSet.TabIndex = 2;
            this.btnSet.Text = "Set Wallpaper";
            this.btnSet.UseVisualStyleBackColor = true;
            this.btnSet.Click += new System.EventHandler(this.btnSet_Click);
            // 
            // flp
            // 
            this.flp.AllowDrop = true;
            this.flp.AutoScroll = true;
            this.flp.AutoSize = true;
            this.flp.BackColor = System.Drawing.SystemColors.Control;
            this.flp.Location = new System.Drawing.Point(0, 0);
            this.flp.Name = "flp";
            this.flp.Size = new System.Drawing.Size(39, 214);
            this.flp.TabIndex = 4;
            this.flp.WrapContents = false;
            // 
            // cboMode
            // 
            this.cboMode.FormattingEnabled = true;
            this.cboMode.Location = new System.Drawing.Point(262, 60);
            this.cboMode.Name = "cboMode";
            this.cboMode.Size = new System.Drawing.Size(121, 21);
            this.cboMode.TabIndex = 6;
            this.cboMode.Text = "Image Mode";
            this.cboMode.SelectedIndexChanged += new System.EventHandler(this.cboMode_SelectedIndexChanged);
            // 
            // pnlControls
            // 
            this.pnlControls.Controls.Add(this.txtPath);
            this.pnlControls.Controls.Add(this.cboMode);
            this.pnlControls.Controls.Add(this.btnSet);
            this.pnlControls.Location = new System.Drawing.Point(12, 239);
            this.pnlControls.Name = "pnlControls";
            this.pnlControls.Size = new System.Drawing.Size(647, 87);
            this.pnlControls.TabIndex = 7;
            // 
            // txtPath
            // 
            this.txtPath.Location = new System.Drawing.Point(122, 17);
            this.txtPath.Name = "txtPath";
            this.txtPath.ReadOnly = true;
            this.txtPath.Size = new System.Drawing.Size(522, 20);
            this.txtPath.TabIndex = 7;
            // 
            // lblCurrent
            // 
            this.lblCurrent.AutoSize = true;
            this.lblCurrent.BackColor = System.Drawing.Color.PaleGreen;
            this.lblCurrent.Location = new System.Drawing.Point(4, 332);
            this.lblCurrent.Name = "lblCurrent";
            this.lblCurrent.Size = new System.Drawing.Size(35, 13);
            this.lblCurrent.TabIndex = 8;
            this.lblCurrent.Text = "label1";
            // 
            // frmWallpaperTool
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(671, 366);
            this.Controls.Add(this.lblCurrent);
            this.Controls.Add(this.pnlControls);
            this.Controls.Add(this.flp);
            this.Name = "frmWallpaperTool";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.pnlControls.ResumeLayout(false);
            this.pnlControls.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnSet;
        private System.Windows.Forms.FlowLayoutPanel flp;
        private System.Windows.Forms.ComboBox cboMode;
        private System.Windows.Forms.Panel pnlControls;
        private System.Windows.Forms.TextBox txtPath;
        private System.Windows.Forms.Label lblCurrent;
    }
}

