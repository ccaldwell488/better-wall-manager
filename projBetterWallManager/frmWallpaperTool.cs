﻿using System;
using System.Windows.Forms;


namespace projBetterWallManager
{
    /**
    * 
    * This will allow users with different sized monitors to create a fitting image by stitching together multiple images to create a large one which will be spanned across all monitors.
    * 
    **/
    public partial class frmWallpaperTool : Form
    {
        ChangeWall Wall = new ChangeWall();
        int monitors;

        // Windows 8 registry values
        enum Modes8
        {
            center = 1,
            stretch = 2,
            fit = 6,
            fill = 10,
            span = 22
        };

        // Win 7 reg vals
        enum Modes7
        {
            center = 0,
            stretch = 2,
            fit = 6,
            fill = 10
        };

        public frmWallpaperTool(int mon)
        {
            this.monitors = mon;
            InitializeComponent();
        }

        /**
         * Dynamically add custom pboxes
         * center panels on the form
         * update label with current wallpaper path
         */
        private void Form1_Load(object sender , EventArgs e)
        {
            // TODO backup reg key
            for (int i = 1; i <= monitors; i++)
            {
                flp.Controls.Add(new CustomPictureBox(this));
            }

            // Center the form after loading custom boxes
            flp.Location = new System.Drawing.Point(this.Width / 2 - flp.Width / 2 , flp.Location.Y);
            pnlControls.Location = new System.Drawing.Point(this.Width / 2 - pnlControls.Width / 2 , pnlControls.Location.Y);
            this.CenterToScreen();

            lblCurrent.Text = "Current Wallpaper: " + Wall.getCurrentWallpaper() + Environment.NewLine + "Current Mode: " + Wall.getCurrentMode();

            cboMode.DataSource = Enum.GetNames(typeof(Modes7));
        }

        private void btnSet_Click(object sender , EventArgs e)
        {
            /** TODO: try this instead of reg keys
             *  https://msdn.microsoft.com/en-us/library/ms724947(VS.85).aspx 
             *  Pinvoke http://pinvoke.net/default.aspx/user32.SystemParametersInfo
             *  
             */

            lblCurrent.Text = "Current Wallpaper: " + Wall.setCurrentWallpaper(txtPath.Text) + Environment.NewLine + "Current Mode: " + Wall.getCurrentMode();

            // Refresh the desktop. Doesn't always work.
            //Process.Start("RUNDLL32.EXE USER32.DLL,UpdatePerUserSystemParameters 1, True");
        }

        // Change Windows registry value to change how the wallpaper is displayed
        private void cboMode_SelectedIndexChanged(object sender , EventArgs e)
        {

            foreach (var v in Enum.GetValues(typeof(Modes7)))
            {
                if (cboMode.SelectedText == v.ToString())
                {
                    //Wall.setWallMode((int)v);
                    MessageBox.Show(v.ToString());
                }
            }

        }

        // Store path of the image that was dropped
        public void itemDropped(string path)
        {
            txtPath.Text = path;
        }
    }
}
